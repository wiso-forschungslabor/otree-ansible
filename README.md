# ansible script to setup multiple [otree](https://otree.readthedocs.io/en/latest/) instances with python env, nginx (as reverse proxy), supervisor (as process manager) and ufw (as firewall)

## preparation/ server setup

### mount a different disk permanently, if you use home_folders not on your root disk 

### request certificate: PKCS#10-method
1. Establish a ssh connection onto your server.
2. Create empty folder ``mkdir /opt/otree/temp`` and switch into ``cd /opt/otree/temp``.
3. Adapt your config file ``request.conf``.
4. Generate encrypted private key and certificate request file with ``openssl req -new -config request.conf -newkey rsa:2048 -sha256 -keyout domain.key.pem.enc -outform PEM -out certrequest.pem`` from config file.
5. Verify your ``certrequest.pem`` with ``openssl req -in certrequest.pem -text``
6. Make a request with the certificate request file at your responsible office.
7. Unencrypt your private key with: ``openssl rsa -in domain.key.pem.enc -out domain.key.pem``.
8. Move your unencrypted private key file with ``sudo mv /opt/otree/temp/private-key.pem /etc/ssl/private/domain.key.pem``.
9. Upload the obtained certificate and the certificate chain onto your server into ``/opt/otree/temp`` and append the chain (PA-Cert) to the certificate with ``cat pa-chain.pem >> cert-myserver.pem``.
10. Lastly move the certificate to the correct location: ``sudo mv /opt/tree/temp/cert-myserver.pem /etc/ssl/certs/domain.crt.pem``.
11. And set the permissions correctly:

``sudo chown root:root /etc/ssl/certs/domain.crt.pem /etc/ssl/private/domain.key.pem``

``sudo chmod 644 /etc/ssl/certs/domain.crt.pem``

``sudo chmod 600 /etc/ssl/private/domain.key.pem``

Note: Use ``scp mycert.crt user@host:/home/user/`` and ``scp mycert.key user@host:/home/user/`` or use [WinSCP](https://de.wikipedia.org/wiki/WinSCP) to copy over the certificate files.

### install ansible and clone this project on your server
1. Establish a ssh connection onto your server.
2. And install git and ansible:

``sudo apt update``

``sudo apt install ansible git``

3. Then clone this ansible project:

``git clone https://gitlab.rrz.uni-hamburg.de/bax1489/otree-ansible.git /opt/otree/otree-ansible/``

### [relocate postgres db, if you're not using your root disk](https://www.digitalocean.com/community/tutorials/how-to-move-a-postgresql-data-directory-to-a-new-location-on-ubuntu-18-04)

## change the configuration of the desired otree server with all installed instances
After setting your desired configuration you are ready to run the playbook.

1. Only on first install: Copy the example-config-file ``cp /opt/otree/otree-ansible/group_vars/otree_servers.yml.example /opt/otree/otree-ansible/group_vars/otree_servers.yml``
2. Adapt your (inventory-)config with ``nano /opt/otree/otree-ansible/group_vars/otree_servers.yml`` from the provided example accordingly (press "Strg-x", then "j" to save):

```
[...]
# fqdn of the otree parents server
otree_domain: "company.org"
# linux group for the otree users
otree_group_name: "otreeusers"

# dict of users for the available otree instances
otree_users:
  # linux username and otree instance name
  otree1:
    # should an existing otree environment be overriden
    override: yes
    # redis queue. Note don't use a slot twice, there are 16 slots available (0-15)
    redis: "redis://localhost:6379/1"
    # otree admin password (also used for postgres db, don't use character "@" or ":" in it)
    admin_pw: "mysecretpw1"
    # auth level, possible values: "STUDY", "DEMO"
    auth_level: "STUDY"
    # django secret key, use differet in different instances
    secret_key: "safaho9awefzuwf"
    # LOG-LEVEL, set to None ("production: ") to enable further debug messages.
    production: "1"
    # port which is used by this instance
    port: 8005
    # python binary used for otree installation
    python_binary_path: "/usr/bin/python3.7"
    # app git-repository (optional value) 
    # app_repo: "https://github.com/oTree-org/oTree.git"  
[...]
```

### get the otree project on your server

#### option A (recommended): clone via git from private repository and provide ssh keys
1. If git-repository ``app_repo`` is provided and the repo is private you have to setup key authentication.
2. Generate a public-private key pair in the ansible project for **every** instance ``/opt/otree/otree-ansible/keys/<otree_instance_name>/`` with ``ssh-keygen -f id -t ed25519 -N '' -a 100``
3. Upload public key in your git provider (GitLab: User Settings->SSH Keys) and make sure you have access to the specified project in ``app_repo``

#### option B: upload otree project directly into ansible project
1. If no git-repository ``app_repo``-value with your otree configuration is provided, you have to place a copy of the project files (with scp) in ``/opt/otree/otree-ansible/files/<otree_username>/``.

## deploy otree instances
> :zap: **Warning:** Make sure there the are **unique** name definitions in the variable ``otree_users`` for the corresponding subdomain (e.g otree5 for otree5.domain.de) in config-file ``group_vars/otree_servers.yml``. (If there are entries with the same key, the last one defined will be deployed!) Make also sure that there are different **secret-keys**, **redis-slots** and **ports** configured for each instance as described above!

1. Change directory: ``cd /opt/otree/otree-ansible/``.
2. Modify/verify your settings again: ``nano /opt/otree/otree-ansible/group_vars/otree_servers.yml``.
3. Run: ``sudo ansible-playbook site.yml`` (with optionally ``-vvv`` to get more information on errors)

> :zap: **Warning:** Set ``override: no`` if the otree_instance is finally deployed and will not change anymore. Remove it only, when you want to **change/delete** the project and deploy it again (Note: This will delete all the data, **always do a backup if you are unsure!**).

## recommendations for otree-code
*  Set a fixed otree-version in ``requirements_base.txt`` and include there all packages defined in variable ``INSTALLED_APPS `` in ``settings.py``.
*  Always set ``SECRET_KEY = environ.get('OTREE_SECRET_KEY')`` in ``settings.py``.
*  Always set ``ADMIN_PASSWORD = environ.get('OTREE_ADMIN_PASSWORD')``
*  Always set ``DEBUG = (environ.get('OTREE_PRODUCTION') in {None, '', '0'})`` in ``settings.py``.

## FAQ/Troubleshooting

### I see "TypeError: __init__() missing 1 required positional argument: 'on_delete'"?
Most likely an older otree version has to be configured. Set ``otree==<desired_version>`` in ``requirements_base.txt``. Since Django 2.0 the "on_delete" parameter is required in a model and needs to be added in your code or in the included apps (e.g otree-tools).

### I see "ModuleNotFoundError: No module named '``<someexperimentname>``'"?
Remove the definition in variable ``SESSION_CONFIGS`` from ``settings.py```or add the corresponding code to your otree project.

### I see "ModuleNotFoundError: No module named 'otree_tools'"?
Add ``otree_tools==<desired_version>`` in ``requirements_base.txt``.

### How can I restart an otree-instance? Where are the log files?
Use supervisor: ``sudo supervisorctl restart <otree_instance_name>``. Log files are located in ``/var/log/supervisor/<otree_instance_name>``.

### How can I restart the web-server nginx? Where are the log files?
Run: ``sudo systemctl restart nginx.service``. All log files are disabled by default due to data protection purposes. Adapt the ngninx template if logging is really necessary.

### For which operating system is this ansible script optimized?
Currently only Ubuntu 18.04.03 LTS with Python 3.6/3.7 is tested and targeted.

### There is an error with redis in apt installation on systems with ipv6 disabled.
Solution: Disable IPV6 also in ``/etc/redis/redis.conf``: ``bind 127.0.0.1``.

### What's the difference to other otree deployment variants?
Other closed/open source projects([oTree Hub](https://www.otreehub.com/), [oTree Virtual Machine Manager](https://otree-virtual-machine-manager.readthedocs.io/en/latest/), [oTree Manager](https://otree-manager.readthedocs.io/en/latest/)) require access to many vms or a dedicated server. This project can be used when there is only one vm in your facility data center. 

### Are there plans for a GUI?
Currently not, but feel free to do feature requests.
